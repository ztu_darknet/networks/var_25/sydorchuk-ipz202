@echo off
echo Task1
systeminfo
pause

echo Task2: ping
ping 192.168.1.1
ping 192.168.1.150
ping 92.253.236.182
pause

echo Task3: ARP
arp -s 192.168.1.150 06-BC-4E-34-6B-7D
arp -a 
arp -d 192.168.1.150
arp -a
pause

echo Task4: Net Time
net time \\DESKTOP-J1IOHQF
pause

echo Task5: Net Use
net use L: \\DESKTOP-J1IOHQF\LAB-15
net use * /delete 
pause

echo Task6
cd C:\
mkdir Sydorchuk_ipz202_25
net share Href=C:\Sydorchuk_ipz202_25
cd C:\Sydorchuk_ipz202_25\
xcopy /y /f %Href% "E:\test lab15.txt"
pause

echo Task7
ping 192.168.1.150
pause
