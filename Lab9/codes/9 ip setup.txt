//R

en
conf t
no ip domain-lookup
int fa9/0
description LINK_TO_WS-2-25-1
ip address 215.2.25.1 255.255.255.240
no shutdown
ex
ip domain-name SYDORCHUK-IPZ202.NET
ex


copy running-config startup-config

//SW-1

en
conf t
int vlan 1
ip address 215.2.25.3 255.255.255.240
no shutdown
ex
ip default-gateway 215.2.25.1
ip name-server 215.2.25.1
ip domain-name SYDORCHUK-IPZ202.NET
ex



//SW-2

en
conf t
int vlan 1
ip address 215.2.25.5 255.255.255.240
no shutdown
ex
ip default-gateway 215.2.25.1
ip name-server 215.2.25.1
ip domain-name SYDORCHUK-IPZ202.NET
ex
