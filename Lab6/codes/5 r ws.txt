//--SW--\\
en
conf t
int fa0/1
description LINK_TO_R-2-25-1
duplex half

speed 100
end


//--R--\\
en
conf t
hostname R-2-25-1
int fa9/0
description LINK_TO_WS-2-25-1
duplex half

speed 100
end
