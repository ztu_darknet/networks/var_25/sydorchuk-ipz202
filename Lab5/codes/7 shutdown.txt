//SW-1
en
conf t
int range fa0/5-24, gig0/2
shutdown

//SW-2
en
conf t
int range fa0/4-24, gig0/2
shutdown
