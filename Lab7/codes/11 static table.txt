//SW-1
en
conf t
mac address-table static 0009.7CD9.E1EB vlan 1 int fa0/1
mac address-table static 0003.E44E.92C3 vlan 1 int gig0/1
mac address-table static 000B.BE65.C124 vlan 1 int gig0/2
mac address-table static 0010.1116.A531 vlan 1 int gig0/1
ex

show mac address-table

//SW-2
en
conf t
mac address-table static 0009.7CD9.E1EB vlan 1 int gig0/1
mac address-table static 0003.E44E.92C3 vlan 1 int gig0/1
mac address-table static 000B.BE65.C124 vlan 1 int gig0/1
mac address-table static 0010.1116.A531 vlan 1 int gig0/2
ex

show mac address-table
